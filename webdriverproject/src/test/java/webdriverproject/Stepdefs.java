package webdriverproject;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.chrome.ChromeDriver;
import static org.junit.Assert.*;

class IsThisSmartbox {
	static String is_this_smartbox(String strUrl) {
		if (strUrl.contains("https://www.smartbox.com/")) {
			return "Yes";
		}
		return "No";
	}
}

public class Stepdefs {
	private String visitedSiteUrl;
	private String actualAnswer;
	
	@Given("^I have visited \"([^\"]*)\"$")
	public void i_have_visited(String arg1) throws Exception {
	    // Write code here that turns the phrase above into concrete actions
	    this.visitedSiteUrl = arg1;
	}

	@When("^I check the URL$")
	public void i_check_the_URL() throws Exception {
	    // Write code here that turns the phrase above into concrete actions
	    this.actualAnswer = IsThisSmartbox.is_this_smartbox(visitedSiteUrl);
	}

	@Then("^I should be told \"([^\"]*)\"$")
	public void i_should_be_told(String expAns) throws Exception {
	    // Write code here that turns the phrase above into concrete actions
	    assertEquals(expAns, actualAnswer);
	}
}