import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * I created this class so that I could create web drivers from a static method
 * @author Ciaran Cumiskey
 *
 */
public class WebDriverFactory {
	public WebDriver createWebDriver() {
		String browserType = System.getProperty("broswer", "firefox");
		switch(browserType) {
			case "firefox":
				return new FirefoxDriver();
			case "chrome":
				return new ChromeDriver();
			default:
				throw new RuntimeException("Unsupported webdriver: " + browserType);
		}
	}
}