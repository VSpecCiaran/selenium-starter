import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Creating web drivers for automated website testing with Selenium
 * @author Ciaran Cumiskey
 *
 */
public class Webdriver {
	public static void main(String[] args) {
		//TODO: Retrieve Geckodriver.exe's location programmatically
		System.setProperty("webdriver.gecko.driver", "C:\\Program Files\\bin\\geckodriver.exe");
		WebDriverFactory wdFactory = new WebDriverFactory();
		WebDriver driver = wdFactory.createWebDriver();
		
		//driver.get("https://www.smartbox.com"); //Visit the Smartbox index page
		driver.get("https://www.smartbox.com/ie/our-smartbox.html"); //Visit the Smartbox products list page
		WebDriverWait wait = new WebDriverWait(driver, 20); //Wait for all of the stuff to load
		//Get all of the blocks corresponding to Smartbox products
		List<WebElement> seeMoreElements = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className("thematic__wrapper-link")));
		//Pick a random block to click on
		seeMoreElements.get(new Random().nextInt(seeMoreElements.size())).click(); //Clicks on that element to take us to the next page
		//Wait until the next page loads, then add that item to the cart
		WebElement cartElement = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("add-to-cart")));
		cartElement.click();
	}
	static String is_this_smartbox(String strUrl) {
		if (strUrl.contains("https://www.smartbox.com/")) {
			return "Yes";
		}
		return "No";
	}
	
}